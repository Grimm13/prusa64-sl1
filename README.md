# PrusA64-SL1

Is a controller board with quad-core Allwinner SoC A64 used in the Prusa SL1 printer.

## Overview

PrusA64-SL1 features:

- Allwinner A64 processor with Quad-Core CortexA53 ARM CPU
- 1GB DDR3 SDRAM and 4GB onboard eMMC flash
- Wifi (802.11b/g/n)
- Ethernet 100 BASE-TX
- USB host and OTG
- 2 lane MIPI DSI interface
- 4 lane MIPI DSI interface (via HDMI-MIPI converter)
- Real-time clock
- Single channel audio out
- SD Card slot

## Convert to KiCad

To view the source you can convert project files into KiCad with opensource tool [altium2kicad](https://github.com/thesourcerer8/altium2kicad). 